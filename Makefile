default: all
.PHONY: all
all:
	sh -c "cd checker; mbld" || true
	sh -c "cd impl-libc; make" || true
	sh -c "cd impl-mpfr; make" || true
	sh -c "cd impl-myrddin; mbld" || true
