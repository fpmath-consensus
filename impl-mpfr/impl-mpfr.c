#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <mpfr.h>

/* Whether we're looking at 32-bit or 64-bit floats */
typedef enum { P_SINGLE, P_DOUBLE } precision;

/* What type of arguments we expect, and what we'll give back. */
typedef enum {
        /* */
        A_UNKNOWN,
        A__FLT__FLT,
        A__FLT_FLT_RND__FLT,
        A__FLT_INT16_RND__FLT,
        A__FLT_UINT16_RND__FLT,
        A__FLT_FLT_FLT_RND__FLT,
        A__FLT_RND__FLT,
} argtype;

/* Types of functions we could call */
typedef int (*f__flt_flt_flt_rnd__flt)(mpfr_ptr, mpfr_srcptr, mpfr_srcptr,
                                       mpfr_srcptr, mpfr_rnd_t);
typedef int (*f__flt__flt)(mpfr_ptr, mpfr_srcptr);
typedef int (*f__flt_flt_rnd__flt)(mpfr_ptr, mpfr_srcptr, mpfr_srcptr,
                                   mpfr_rnd_t);
typedef int (*f__flt_int16_rnd__flt)(mpfr_ptr, mpfr_srcptr, long int,
                                     mpfr_rnd_t);
typedef int (*f__flt_uint16_rnd__flt)(mpfr_ptr, mpfr_srcptr, unsigned long int,
                                      mpfr_rnd_t);
typedef int (*f__flt_rnd__flt)(mpfr_ptr, mpfr_srcptr, mpfr_rnd_t);

/* Wrapper around a function pointer */
typedef struct {
        /* */
        precision p;
        argtype a;

        union {
                /* */
                f__flt__flt flt__flt;
                f__flt_flt_rnd__flt flt_flt_rnd__flt;
                f__flt_int16_rnd__flt flt_int16_rnd__flt;
                f__flt_uint16_rnd__flt flt_uint16_rnd__flt;
                f__flt_flt_flt_rnd__flt flt_flt_flt_rnd__flt;
                f__flt_rnd__flt flt_rnd__flt;
        } f;
} action;

void
usage(void)
{
        fprintf(stderr,
                "usage: impl-mpfr [-s|-d] -f <function_name> -n <num_inputs>\n");
        _exit(1);
}

void
determine_function(const char *f, action *a)
{
        if (!strcmp(f, "zzzzzzzzz")) {
                a->a = A_UNKNOWN;
        } else if (!strcmp(f, "id")) {
                a->a = A__FLT_RND__FLT;
                a->f.flt_rnd__flt = mpfr_set;
        } else if (!strcmp(f, "atan")) {
                a->a = A__FLT_RND__FLT;
                a->f.flt_rnd__flt = mpfr_atan;
        } else if (!strcmp(f, "atan2")) {
                a->a = A__FLT_FLT_RND__FLT;
                a->f.flt_flt_rnd__flt = mpfr_atan2;
        } else if (!strcmp(f, "ceil")) {
                a->a = A__FLT__FLT;
                a->f.flt__flt = mpfr_ceil;
        } else if (!strcmp(f, "cos")) {
                a->a = A__FLT_RND__FLT;
                a->f.flt_rnd__flt = mpfr_cos;
        } else if (!strcmp(f, "cot")) {
                a->a = A__FLT_RND__FLT;
                a->f.flt_rnd__flt = mpfr_cot;
        } else if (!strcmp(f, "floor")) {
                a->a = A__FLT__FLT;
                a->f.flt__flt = mpfr_floor;
        } else if (!strcmp(f, "exp")) {
                a->a = A__FLT_RND__FLT;
                a->f.flt_rnd__flt = mpfr_exp;
        } else if (!strcmp(f, "expm1")) {
                a->a = A__FLT_RND__FLT;
                a->f.flt_rnd__flt = mpfr_expm1;
        } else if (!strcmp(f, "fma")) {
                a->a = A__FLT_FLT_FLT_RND__FLT;
                a->f.flt_flt_flt_rnd__flt = mpfr_fma;
        } else if (!strcmp(f, "log")) {
                a->a = A__FLT_RND__FLT;
                a->f.flt_rnd__flt = mpfr_log;
        } else if (!strcmp(f, "log1p")) {
                a->a = A__FLT_RND__FLT;
                a->f.flt_rnd__flt = mpfr_log1p;
        } else if (!strcmp(f, "pown")) {
                a->a = A__FLT_INT16_RND__FLT;
                a->f.flt_int16_rnd__flt = mpfr_pow_si;
        } else if (!strcmp(f, "powr")) {
                a->a = A__FLT_FLT_RND__FLT;
                a->f.flt_flt_rnd__flt = mpfr_pow;
        } else if (!strcmp(f, "rootn")) {
                a->a = A__FLT_UINT16_RND__FLT;
                a->f.flt_uint16_rnd__flt = mpfr_rootn_ui;
        } else if (!strcmp(f, "sin")) {
                a->a = A__FLT_RND__FLT;
                a->f.flt_rnd__flt = mpfr_sin;
        } else if (!strcmp(f, "sqrt")) {
                a->a = A__FLT_RND__FLT;
                a->f.flt_rnd__flt = mpfr_sqrt;
        } else if (!strcmp(f, "tan")) {
                a->a = A__FLT_RND__FLT;
                a->f.flt_rnd__flt = mpfr_tan;
        } else if (!strcmp(f, "trunc")) {
                a->a = A__FLT__FLT;
                a->f.flt__flt = mpfr_trunc;
        } else {
                fprintf(stderr, "impl-mpfr: unknown function \"%s\"\n", f);
                _exit(1);
        }
}

void
read_buf(char *b, ssize_t len)
{
        ssize_t r;
        ssize_t total = 0;

        while (total < len) {
                r = read(0, (b + total), (len - total));

                if (!r) {
                        /* EOF */
                        _exit(0);
                } else if (r == -1) {
                        perror("impl-mpfr: read");
                        _exit(1);
                } else {
                        total += r;
                }
        }
}

void
write_buf(const char *b, ssize_t len)
{
        ssize_t r;
        ssize_t total = 0;

        while (total < len) {
                r = write(1, (b + total), (len - total));

                if (r == -1) {
                        perror("impl-mpfr: write");
                        _exit(1);
                } else {
                        total += r;
                }
        }
}

size_t
input_width(argtype a, precision p)
{
        size_t w = (p == P_SINGLE) ? 4 : 8;

        switch (a) {
        case A_UNKNOWN:
                break;
        case A__FLT__FLT:

                return 1 * w;
        case A__FLT_FLT_RND__FLT:

                return 2 * w;
        case A__FLT_INT16_RND__FLT:

                return w + 2;
        case A__FLT_UINT16_RND__FLT:

                return w + 2;
        case A__FLT_FLT_FLT_RND__FLT:

                return 3 * w;
        case A__FLT_RND__FLT:

                return 1 * w;
        }

        return (size_t) -1;
}

size_t
output_width(argtype a, precision p)
{
        size_t w = (p == P_SINGLE) ? 4 : 8;

        switch (a) {
        case A_UNKNOWN:
                break;
        case A__FLT__FLT:

                return 1 * w;
        case A__FLT_FLT_RND__FLT:

                return 1 * w;
        case A__FLT_INT16_RND__FLT:

                return 1 * w;
        case A__FLT_UINT16_RND__FLT:

                return 1 * w;
        case A__FLT_FLT_FLT_RND__FLT:

                return 1 * w;
        case A__FLT_RND__FLT:

                return 1 * w;
        }

        return (size_t) -1;
}

void
io_loop(action a, size_t n)
{
        char *in_buf = 0;
        char *out_buf = 0;
        size_t in_sz = input_width(a.a, a.p);
        size_t out_sz = output_width(a.a, a.p);
        mpfr_t x1;
        mpfr_t x2;
        mpfr_t x3;
        mpfr_t y;

        if ((in_sz * n) / n != in_sz) {
                fprintf(stderr, "impl-mpfr: input length overflow\n");
                _exit(1);
        }

        if ((out_sz * n) / n != out_sz) {
                fprintf(stderr, "impl-mpfr: output length overflow\n");
                _exit(1);
        }

        if (!(in_buf = malloc(in_sz * n))) {
                perror("impl-mpfr: malloc");
                _exit(1);
        }

        if (!(out_buf = malloc(out_sz * n))) {
                perror("impl-mpfr: malloc");
                _exit(1);
        }

        /* I'm pretty sure 53 precision would be enough */
        mpfr_init2(x1, 75);
        mpfr_init2(x2, 75);
        mpfr_init2(x3, 75);
        mpfr_init2(y, 75);

        while (1) {
                read_buf(in_buf, in_sz * n);

                switch (a.a) {
                case A_UNKNOWN:
                        fprintf(stderr, "impl-mpfr: impossible\n");
                        _exit(1);
                        break;
                case A__FLT__FLT:

                        switch (a.p) {
                        case P_SINGLE:

                                for (size_t j = 0; j < n; ++j) {
                                        float *xf1 = (float *) (in_buf +
                                                                (in_sz * j));
                                        float *yf = (float *) (out_buf +
                                                               (out_sz * j));

                                        mpfr_set_flt(x1, *xf1, MPFR_RNDN);
                                        a.f.flt__flt(y, x1);
                                        *yf = mpfr_get_flt(y, MPFR_RNDN);
                                }

                                break;
                        case P_DOUBLE:

                                for (size_t j = 0; j < n; ++j) {
                                        double *xf1 = (double *) (in_buf +
                                                                  (in_sz * j));
                                        double *yf = (double *) (out_buf +
                                                                 (out_sz * j));

                                        mpfr_set_d(x1, *xf1, MPFR_RNDN);
                                        a.f.flt__flt(y, x1);
                                        *yf = mpfr_get_d(y, MPFR_RNDN);
                                }

                                break;
                        }

                        break;
                case A__FLT_FLT_RND__FLT:

                        switch (a.p) {
                        case P_SINGLE:

                                for (size_t j = 0; j < n; ++j) {
                                        float *xf1 = (float *) (in_buf +
                                                                (in_sz * j));
                                        float *xf2 = (float *) (in_buf +
                                                                (in_sz * j) +
                                                                4);
                                        float *yf = (float *) (out_buf +
                                                               (out_sz * j));

                                        mpfr_set_flt(x1, *xf1, MPFR_RNDN);
                                        mpfr_set_flt(x2, *xf2, MPFR_RNDN);
                                        a.f.flt_flt_rnd__flt(y, x1, x2,
                                                             MPFR_RNDN);
                                        *yf = mpfr_get_flt(y, MPFR_RNDN);
                                }

                                break;
                        case P_DOUBLE:

                                for (size_t j = 0; j < n; ++j) {
                                        double *xf1 = (double *) (in_buf +
                                                                  (in_sz * j));
                                        double *xf2 = (double *) (in_buf +
                                                                  (in_sz * j) +
                                                                  8);
                                        double *yf = (double *) (out_buf +
                                                                 (out_sz * j));

                                        mpfr_set_d(x1, *xf1, MPFR_RNDN);
                                        mpfr_set_d(x2, *xf2, MPFR_RNDN);
                                        a.f.flt_flt_rnd__flt(y, x1, x2,
                                                             MPFR_RNDN);
                                        *yf = mpfr_get_d(y, MPFR_RNDN);
                                }

                                break;
                        }

                        break;
                case A__FLT_INT16_RND__FLT:

                        switch (a.p) {
                        case P_SINGLE:

                                for (size_t j = 0; j < n; ++j) {
                                        float *xf1 = (float *) (in_buf +
                                                                (in_sz * j));
                                        int16_t *xi2 = (int16_t *) (in_buf +
                                                                    (in_sz *
                                                                     j) + 4);
                                        float *yf = (float *) (out_buf +
                                                               (out_sz * j));

                                        mpfr_set_flt(x1, *xf1, MPFR_RNDN);
                                        a.f.flt_int16_rnd__flt(y, x1, (long
                                                                       int) *xi2,
                                                               MPFR_RNDN);
                                        *yf = mpfr_get_flt(y, MPFR_RNDN);
                                }

                                break;
                        case P_DOUBLE:

                                for (size_t j = 0; j < n; ++j) {
                                        double *xf1 = (double *) (in_buf +
                                                                  (in_sz * j));
                                        int16_t *xi2 = (int16_t *) (in_buf +
                                                                    (in_sz *
                                                                     j) + 8);
                                        double *yf = (double *) (out_buf +
                                                                 (out_sz * j));

                                        mpfr_set_d(x1, *xf1, MPFR_RNDN);
                                        a.f.flt_int16_rnd__flt(y, x1, (long
                                                                       int) *xi2,
                                                               MPFR_RNDN);
                                        *yf = mpfr_get_d(y, MPFR_RNDN);
                                }

                                break;
                        }

                        break;
                case A__FLT_UINT16_RND__FLT:

                        switch (a.p) {
                        case P_SINGLE:

                                for (size_t j = 0; j < n; ++j) {
                                        float *xf1 = (float *) (in_buf +
                                                                (in_sz * j));
                                        uint16_t *xi2 = (uint16_t *) (in_buf +
                                                                      (in_sz *
                                                                       j) + 4);
                                        float *yf = (float *) (out_buf +
                                                               (out_sz * j));

                                        mpfr_set_flt(x1, *xf1, MPFR_RNDN);
                                        a.f.flt_uint16_rnd__flt(y, x1, (unsigned
                                                                        long int)
                                                                *xi2,
                                                                MPFR_RNDN);
                                        *yf = mpfr_get_flt(y, MPFR_RNDN);
                                }

                                break;
                        case P_DOUBLE:

                                for (size_t j = 0; j < n; ++j) {
                                        double *xf1 = (double *) (in_buf +
                                                                  (in_sz * j));
                                        uint16_t *xi2 = (uint16_t *) (in_buf +
                                                                      (in_sz *
                                                                       j) + 8);
                                        double *yf = (double *) (out_buf +
                                                                 (out_sz * j));

                                        mpfr_set_d(x1, *xf1, MPFR_RNDN);
                                        a.f.flt_uint16_rnd__flt(y, x1, (unsigned
                                                                        long int)
                                                                *xi2,
                                                                MPFR_RNDN);
                                        *yf = mpfr_get_d(y, MPFR_RNDN);
                                }

                                break;
                        }

                        break;
                case A__FLT_FLT_FLT_RND__FLT:

                        switch (a.p) {
                        case P_SINGLE:

                                for (size_t j = 0; j < n; ++j) {
                                        float *xf1 = (float *) (in_buf +
                                                                (in_sz * j));
                                        float *xf2 = (float *) (in_buf +
                                                                (in_sz * j) +
                                                                4);
                                        float *xf3 = (float *) (in_buf +
                                                                (in_sz * j) +
                                                                8);
                                        float *yf = (float *) (out_buf +
                                                               (out_sz * j));

                                        mpfr_set_flt(x1, *xf1, MPFR_RNDN);
                                        mpfr_set_flt(x2, *xf2, MPFR_RNDN);
                                        mpfr_set_flt(x3, *xf3, MPFR_RNDN);
                                        a.f.flt_flt_flt_rnd__flt(y, x1, x2, x3,
                                                                 MPFR_RNDN);
                                        *yf = mpfr_get_flt(y, MPFR_RNDN);
                                }

                                break;
                        case P_DOUBLE:

                                for (size_t j = 0; j < n; ++j) {
                                        double *xf1 = (double *) (in_buf +
                                                                  (in_sz * j));
                                        double *xf2 = (double *) (in_buf +
                                                                  (in_sz * j) +
                                                                  8);
                                        double *xf3 = (double *) (in_buf +
                                                                  (in_sz * j) +
                                                                  16);
                                        double *yf = (double *) (out_buf +
                                                                 (out_sz * j));

                                        mpfr_set_d(x1, *xf1, MPFR_RNDN);
                                        mpfr_set_d(x2, *xf2, MPFR_RNDN);
                                        mpfr_set_d(x3, *xf3, MPFR_RNDN);
                                        a.f.flt_flt_flt_rnd__flt(y, x1, x2, x3,
                                                                 MPFR_RNDN);
                                        *yf = mpfr_get_d(y, MPFR_RNDN);
                                }

                                break;
                        }

                        break;
                case A__FLT_RND__FLT:

                        switch (a.p) {
                        case P_SINGLE:

                                for (size_t j = 0; j < n; ++j) {
                                        float *xf1 = (float *) (in_buf +
                                                                (in_sz * j));
                                        float *yf = (float *) (out_buf +
                                                               (out_sz * j));

                                        mpfr_set_flt(x1, *xf1, MPFR_RNDN);
                                        a.f.flt_rnd__flt(y, x1, MPFR_RNDN);
                                        *yf = mpfr_get_flt(y, MPFR_RNDN);
                                }

                                break;
                        case P_DOUBLE:

                                for (size_t j = 0; j < n; ++j) {
                                        double *xf1 = (double *) (in_buf +
                                                                  (in_sz * j));
                                        double *yf = (double *) (out_buf +
                                                                 (out_sz * j));

                                        mpfr_set_d(x1, *xf1, MPFR_RNDN);
                                        a.f.flt_rnd__flt(y, x1, MPFR_RNDN);
                                        *yf = mpfr_get_d(y, MPFR_RNDN);
                                }

                                break;
                        }

                        break;
                }

                write_buf(out_buf, out_sz * n);
        }
}

int
main(int argc, char **argv)
{
        int c = 0;
        action a = { .p = P_SINGLE };
        long long n = 0;

        while ((c = getopt(argc, argv, "sdf:n:")) != -1) {
                switch (c) {
                case 's':
                        a.p = P_SINGLE;
                        break;
                case 'd':
                        a.p = P_DOUBLE;
                        break;
                case 'f':
                        determine_function(optarg, &a);
                        break;
                case 'n':
                        errno = 0;
                        n = strtoll(optarg, 0, 0);

                        if (errno) {
                                perror("impl-mpfr: unparsable");

                                return 1;
                        }

                        break;
                default:
                        usage();
                        break;
                }
        }

        if (a.a == A_UNKNOWN) {
                usage();
        }

        io_loop(a, n);
}
