#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

/* Whether we're looking at 32-bit or 64-bit floats */
typedef enum { P_SINGLE, P_DOUBLE } precision;

/* What type of arguments we expect, and what we'll give back. */
typedef enum {
        /* */
        A_UNKNOWN,
        A__FLT__FLT,
        A__FLT_FLT__FLT,
        A__FLT_FLT_FLT__FLT,
} argtype;

/* Types of functions we could call */
typedef float (*f__f32__f32)(float);
typedef float (*f__f32_f32__f32)(float, float);
typedef float (*f__f32_f32_f32__f32)(float, float, float);
typedef double (*f__f64__f64)(double);
typedef double (*f__f64_f64__f64)(double, double);
typedef double (*f__f64_f64_f64__f64)(double, double, double);

/* Wrapper around a function pointer */
typedef struct {
        /* */
        precision p;
        argtype a;

        union {
                /* */
                f__f32__f32 f32__f32;
                f__f32_f32__f32 f32_f32__f32;
                f__f32_f32_f32__f32 f32_f32_f32__f32;
        } f32;

        union {
                /* */
                f__f64__f64 f64__f64;
                f__f64_f64__f64 f64_f64__f64;
                f__f64_f64_f64__f64 f64_f64_f64__f64;
        } f64;

} action;

void usage(void)
{
        fprintf(stderr,
                "usage: impl-libc [-s|-d] -f <function_name> -n <num_inputs>\n");
        _exit(1);
}

float idf(float f)
{
        return f;
}

double idd(double d)
{
        return d;
}

void determine_function(const char *f, action *a)
{
        if (!strcmp(f, "zzzzzz")) {
                a->a = A_UNKNOWN;
        } else if (!strcmp(f, "id")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = idf;
                a->f64.f64__f64 = idd;
        } else if (!strcmp(f, "atan")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = atanf;
                a->f64.f64__f64 = atan;
        } else if (!strcmp(f, "atan2")) {
                a->a = A__FLT_FLT__FLT;
                a->f32.f32_f32__f32 = atan2f;
                a->f64.f64_f64__f64 = atan2;
        } else if (!strcmp(f, "ceil")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = ceilf;
                a->f64.f64__f64 = ceil;
        } else if (!strcmp(f, "cos")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = cosf;
                a->f64.f64__f64 = cos;
        } else if (!strcmp(f, "cot")) {
                a->a = A_UNKNOWN;
        } else if (!strcmp(f, "floor")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = floorf;
                a->f64.f64__f64 = floor;
        } else if (!strcmp(f, "fma")) {
                a->a = A__FLT_FLT_FLT__FLT;
                a->f32.f32_f32_f32__f32 = fmaf;
                a->f64.f64_f64_f64__f64 = fma;
        } else if (!strcmp(f, "exp")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = expf;
                a->f64.f64__f64 = exp;
        } else if (!strcmp(f, "expm1")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = expm1f;
                a->f64.f64__f64 = expm1;
        } else if (!strcmp(f, "log")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = logf;
                a->f64.f64__f64 = log;
        } else if (!strcmp(f, "log1p")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = log1pf;
                a->f64.f64__f64 = log1p;
        } else if (!strcmp(f, "powr") || !strcmp(f, "pown") || !strcmp(f, "rootn")) {
                /* libcs don't seem to have powr (yet?), just pow */
                a->a = A_UNKNOWN;
        } else if (!strcmp(f, "sin")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = sinf;
                a->f64.f64__f64 = sin;
        } else if (!strcmp(f, "sqrt")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = sqrtf;
                a->f64.f64__f64 = sqrt;
        } else if (!strcmp(f, "tan")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = tanf;
                a->f64.f64__f64 = tan;
        } else if (!strcmp(f, "trunc")) {
                a->a = A__FLT__FLT;
                a->f32.f32__f32 = truncf;
                a->f64.f64__f64 = trunc;
        } else {
                fprintf(stderr, "impl-libc: unknown function \"%s\"\n", f);
                _exit(1);
        }
}

void read_buf(char *b, ssize_t len)
{
        ssize_t r;
        ssize_t total = 0;

        while (total < len) {
                r = read(0, (b + total), (len - total));

                if (!r) {
                        /* EOF */
                        _exit(0);
                } else if (r == -1) {
                        perror("impl-libc: read");
                        _exit(1);
                } else {
                        total += r;
                }
        }
}

void write_buf(const char *b, ssize_t len)
{
        ssize_t r;
        ssize_t total = 0;

        while (total < len) {
                r = write(1, (b + total), (len - total));

                if (r == -1) {
                        perror("impl-libc: write");
                        _exit(1);
                } else {
                        total += r;
                }
        }
}

size_t input_width(argtype a, precision p)
{
        size_t w = (p == P_SINGLE) ? 4 : 8;

        switch (a) {
        case A_UNKNOWN:
                break;
        case A__FLT__FLT:

                return 1 * w;
        case A__FLT_FLT__FLT:

                return 2 * w;
        case A__FLT_FLT_FLT__FLT:

                return 3 * w;
        }

        return (size_t) -1;
}

size_t output_width(argtype a, precision p)
{
        size_t w = (p == P_SINGLE) ? 4 : 8;

        switch (a) {
        case A_UNKNOWN:
                break;
        case A__FLT__FLT:

                return 1 * w;
        case A__FLT_FLT__FLT:

                return 1 * w;
        case A__FLT_FLT_FLT__FLT:

                return 1 * w;
        }

        return (size_t) -1;
}

void io_loop(action a, size_t n)
{
        char *in_buf = 0;
        char *out_buf = 0;
        size_t in_sz = input_width(a.a, a.p);
        size_t out_sz = output_width(a.a, a.p);

        if ((in_sz * n) / n != in_sz) {
                fprintf(stderr, "impl-libc: input length overflow\n");
                _exit(1);
        }

        if ((out_sz * n) / n != out_sz) {
                fprintf(stderr, "impl-libc: output length overflow\n");
                _exit(1);
        }

        if (!(in_buf = malloc(in_sz * n))) {
                perror("impl-libc: malloc");
                _exit(1);
        }

        if (!(out_buf = malloc(out_sz * n))) {
                perror("impl-libc: malloc");
                _exit(1);
        }

        while (1) {
                read_buf(in_buf, in_sz * n);

                switch (a.a) {
                case A_UNKNOWN:
                        fprintf(stderr, "impl-libc: impossible\n");
                        _exit(1);
                        break;
                case A__FLT__FLT:

                        switch (a.p) {
                        case P_SINGLE:

                                for (size_t j = 0; j < n; ++j) {
                                        float *x = (float *) (in_buf + (in_sz *
                                                                        j));
                                        float *y = (float *) (out_buf +
                                                              (out_sz * j));

                                        *y = a.f32.f32__f32(*x);
                                }

                                break;
                        case P_DOUBLE:

                                for (size_t j = 0; j < n; ++j) {
                                        double *x = (double *) (in_buf +
                                                                (in_sz * j));
                                        double *y = (double *) (out_buf +
                                                                (out_sz * j));

                                        *y = a.f64.f64__f64(*x);
                                }

                                break;
                        }

                        break;
                case A__FLT_FLT__FLT:

                        switch (a.p) {
                        case P_SINGLE:

                                for (size_t j = 0; j < n; ++j) {
                                        float *x1 = (float *) (in_buf + (in_sz *
                                                                         j));
                                        float *x2 = (float *) (in_buf + (in_sz *
                                                                         j +
                                                                         4));
                                        float *y = (float *) (out_buf +
                                                              (out_sz * j));

                                        *y = a.f32.f32_f32__f32(*x1, *x2);
                                }

                                break;
                        case P_DOUBLE:

                                for (size_t j = 0; j < n; ++j) {
                                        double *x1 = (double *) (in_buf +
                                                                 (in_sz * j));
                                        double *x2 = (double *) (in_buf +
                                                                 (in_sz * j) +
                                                                 8);
                                        double *y = (double *) (out_buf +
                                                                (out_sz * j));

                                        *y = a.f64.f64_f64__f64(*x1, *x2);
                                }

                                break;
                        }

                        break;
                case A__FLT_FLT_FLT__FLT:

                        switch (a.p) {
                        case P_SINGLE:

                                for (size_t j = 0; j < n; ++j) {
                                        float *x1 = (float *) (in_buf + (in_sz *
                                                                         j));
                                        float *x2 = (float *) (in_buf + (in_sz *
                                                                         j +
                                                                         4));
                                        float *x3 = (float *) (in_buf + (in_sz *
                                                                         j +
                                                                         8));
                                        float *y = (float *) (out_buf +
                                                              (out_sz * j));

                                        *y = a.f32.f32_f32_f32__f32(*x1, *x2,
                                                                    *x3);
                                }

                                break;
                        case P_DOUBLE:

                                for (size_t j = 0; j < n; ++j) {
                                        double *x1 = (double *) (in_buf +
                                                                 (in_sz * j));
                                        double *x2 = (double *) (in_buf +
                                                                 (in_sz * j) +
                                                                 8);
                                        double *x3 = (double *) (in_buf +
                                                                 (in_sz * j) +
                                                                 16);
                                        double *y = (double *) (out_buf +
                                                                (out_sz * j));

                                        *y = a.f64.f64_f64_f64__f64(*x1, *x2,
                                                                    *x3);
                                }

                                break;
                        }

                        break;
                }

                write_buf(out_buf, out_sz * n);
        }
}

int main(int argc, char **argv)
{
        int c = 0;
        action a = { .p = P_SINGLE };
        long long n = 0;

        while ((c = getopt(argc, argv, "sdf:n:")) != -1) {
                switch (c) {
                case 's':
                        a.p = P_SINGLE;
                        break;
                case 'd':
                        a.p = P_DOUBLE;
                        break;
                case 'f':
                        determine_function(optarg, &a);
                        break;
                case 'n':
                        errno = 0;
                        n = strtoll(optarg, 0, 0);

                        if (errno) {
                                perror("impl-libc: unparsable");

                                return 1;
                        }

                        break;
                default:
                        usage();
                        break;
                }
        }

        if (a.a == A_UNKNOWN) {
                usage();
        }

        io_loop(a, n);
}
